<?php

  // TAKE the code passed by POST

  if (isset($_POST['code_model'])) {
	$code_model = $_POST['code_model'];
	$code_refinement = $_POST['code_refinement'];
	$code_initial_state = $_POST['code_initial_state'];
	$code_scenario = $_POST['code_scenario'];
  }
  
  $date = date("Y.m.d_H.i");
  
  if (isset($_POST['filename'])) {
	$filename = $_POST['filename'].".".$date;		
  } else {
	$filename = $date;
  }
  
  header("Content-Type: text/plain");
  header("Content-Disposition: Attachment; filename=$filename.eflint");
  header("Pragma: no-cache");

  print("#\n\n$code_model\n\n##\n\n$code_refinement\n\n###\n\n$code_initial_state\n\n####\n\n$code_scenario");
  
  exit();
