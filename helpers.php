<?php

function my_trim($string) {
	$string = preg_replace('/^[\n\t\s]*/', '', $string);
    $string = preg_replace('/[\n\t\s]*$/', '', $string);
    return $string;
}

function get_file($filename) {
  if (file_exists($filename)) {
	$code = implode("", file($filename));	
	
	$pos = strrpos($code, "####");
	if ($pos === false) { 
		$code_scenario = "";
	}
	else {
		$code_scenario = substr($code, $pos+4);
		$code = substr($code, 0, $pos);
	}
	
	$pos = strrpos($code, "###");
	if ($pos === false) { 
		$code_initial_state = "";
	}
	else {
		$code_initial_state = substr($code, $pos+3);
		$code = substr($code, 0, $pos);
	}
	
	$pos = strrpos($code, "##");
	if ($pos === false) { 
		$code_refinement = "";
	}
	else {
		$code_refinement = substr($code, $pos+2);
		$code = substr($code, 0, $pos);
	}
  $code_model = my_trim(str_replace("#", "", $code));
  $code_refinement = my_trim($code_refinement);
  $code_initial_state = my_trim($code_initial_state);
  $code_scenario = my_trim($code_scenario);
  }
  else {
    $code_model = "// Sorry, file <$filename> not found.";
    $code_refinement = "// Sorry, file not found.";
    $code_initial_state = "// Sorry, file not found.";
    $code_scenario = "// Sorry, file not found.";
  }
	return array("code_model" => $code_model, "code_refinement" => $code_refinement, "code_initial_state" => $code_initial_state, "code_scenario" => $code_scenario);
}
?>
