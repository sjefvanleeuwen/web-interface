<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function write_file($filename, $content) {	
	$f = fopen("$filename", "w");
	if ($f === false) return false;
	fwrite($f, $content);
	fclose($f);
	return true;
}

  // RUN the code passed by POST

  $result = "";
  if (isset($_POST['code_model'])) {
	$code_model = $_POST['code_model'];
	$code_refinement = $_POST['code_refinement'];
	$code_initial_state = $_POST['code_initial_state'];
	$code_scenario = $_POST['code_scenario'];
		
	$md5 = md5($code_model.$code_refinement.$code_initial_state.$code_scenario);

    // to avoid the consequent removal of files being used				
	while (file_exists("./tmp/$md5.model")) { $md5++; };
	$return = "";
	
	if (!write_file("./tmp/$md5.model", $code_model)) {
	  $return = "Unable to write file!";
	}
    
    if (!$return) { 
	  if (!write_file("./tmp/$md5.refinement", $code_refinement)) {
		$return = "Unable to write file!";
	  }
    }

    if (!$return) { 
	  if (!write_file("./tmp/$md5.initial_state", $code_initial_state)) {
		$return = "Unable to write file!";
	  }
    }

    if (!$return) { 
	  if (!write_file("./tmp/$md5.scenario", $code_scenario)) {
		$return = "Unable to write file!";
	  }
    }

    
    		         
    if (!$return) { 
	  exec("./flint-simulation ./tmp/$md5.model ./tmp/$md5.refinement ./tmp/$md5.initial_state ./tmp/$md5.scenario --positive-test --json > ./tmp/$md5.output 2> ./tmp/$md5.error ", $output, $return);      
    }
    
    if (file_exists("./tmp/$md5.error")) 
      exec("cat ./tmp/$md5.error", $output);

    exec("cat ./tmp/$md5.output", $output);
     
    # exec("rm ./tmp/$md5.error");
    exec("rm ./tmp/$md5.model");
    exec("rm ./tmp/$md5.refinement");
    exec("rm ./tmp/$md5.initial_state");
    exec("rm ./tmp/$md5.scenario");
    exec("rm ./tmp/$md5.output");
    
    $result = implode($output, "\n");
  }
 
print(json_encode($result));
