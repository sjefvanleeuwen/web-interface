#!/bin/bash

TESTER=$1
export TESTER

run_test() { 
  OUTFILE="$1.output"
  $TESTER $1 --positive-test --json | jq . > $OUTFILE
}
export -f run_test
find . -type f \( -iname \*.eflint \) | xargs -n1 -I{} bash -c 'run_test "$@"' _ {} 
