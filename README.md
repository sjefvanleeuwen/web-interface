A webinterface for accessing the eFLINT prototype-implementation.

Requires:

* php (tested with version 7)
* writing rights for the `www-data` user on the folder `tmp/`
* the `flint-simulation` executable, with execution right for `www-data`, produced by the eFLINT prototype implementation (placed at the top-level, next to index.php)

examples ready for loading and running are in the folder `examples/`