<?php
require_once("helpers.php");
if (isset($_FILES["fileToUpload"]["tmp_name"])) {
  $filename = $_FILES["fileToUpload"]["tmp_name"];
  $arr = get_file($filename);
  $code_model = $arr['code_model'];
  $code_refinement = $arr['code_refinement'];
  $code_initial_state = $arr['code_initial_state'];
  $code_scenario = $arr['code_scenario'];
}
?>
<html> 
<head> 
<title>eFLINT online!</title> 
<script type="text/javascript" src="./jquery-1.8.2.min.js"></script>

<script type='text/javascript'>

function hasJsonStructure(str) {
    if (typeof str !== 'string') return false;
    try {
        const result = JSON.parse(str);
        const type = Object.prototype.toString.call(result);
        return type === '[object Object]' 
            || type === '[object Array]';
    } catch (err) {
        return false;
    }
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

function startFileLoad(file) {
    $.post("load.php", {"filename": file}, function(result) {
        handleFileLoad(result);
      }, "json")
}

function handleFileLoad(result) {
  $("#result").html("");    
  $("#console").val("");
  $("#code_model").val(result["code_model"]);
  $("#code_refinement").val(result["code_refinement"]);
  $("#code_initial_state").val(result["code_initial_state"]);
  $("#code_scenario").val(result["code_scenario"]);
  // $("#examples").hide(); showexample = false;
  $("div.step").remove();
  $("#response").find("pre").html("");
}

function addStateContents(state, contents) {
  contents.forEach(function(item) {
    state.append("<div class='tagged-elem'>" + item.textual + "</div>"); 
  }); 
}

$(document).ready(function() {
	
  var showexample = false;
	
  // $("#examples").hide();
  
  // $("#showexamples").click(function() {
  //	if (!showexample) { $("#examples").show(); showexample = true; }
  //      else { $("#examples").hide(); showexample = false; }
  // });

  var response_field = $("#response").find("pre");
  var steps = $("#output");
 $("#run").click(function() {
 	var code_model = $("#code_model").val();
	var code_refinement = $("#code_refinement").val();
	var code_initial_state = $("#code_initial_state").val();
	var code_scenario = $("#code_scenario").val();
  response_field.val("");
	$("#console").val("");
	$("#result").html("");
  $("div.step").remove();    
    $(".right").addClass("transparent");

    $.post("run.php", {"code_model": code_model, 
		"code_refinement": code_refinement, 
		"code_initial_state": code_initial_state,
		"code_scenario": code_scenario}, function(result) {
	  $(".right").removeClass("transparent");
	  if (!hasJsonStructure(result)) {
	    response_field.html("technical error");
      $("#command").html("<h2>command output (hidden)</h2><pre class='toggleitem'>"+result+"</pre>");
      $("#command").find("pre").hide();
      $("#command").find("h2").click(function() {
         $(this).parent().find("pre").toggle();
      });
	  } else {
      obj = JSON.parse(result);
      if (obj.errors.length == 0) {
        response_field.html("ok");  
      } else {
        response_field.html("");
        obj.errors.forEach(function(item) {
         response_field.append("* " + item + "\n"); 
        });
      }

      var step0 = $("<div class='step step0'></div>");
      steps.append(step0); 
      step0.append("<div class='action'><div class='instance'>Step 0: initial state</div></div>");
      state = $("<div class='state'></div>");
      step0.append(state);
      addStateContents(state, obj.reached_states[0][1]);

      obj.exec_actions.forEach(function(item, index) {
        step = $("<div class='step'></div>").addClass("step" + item[0]);
        steps.append(step);
        var step_info = null == item[1] ? " query" : item[1].instance.textual;
        action = $("<div class='action'><div class='instance'>Step " + item[0] + ": " + step_info + "</div></div>");
        step.append(action);
        if (null != item[1]) {
          item[1].creates.forEach(function(c) {
            action.append($("<div class='tagged-elem created'>+" + c.textual + "</div>"));
          });
          item[1].terminates.forEach(function(t) {
            action.append($("<div class='tagged-elem terminated'>-" + t.textual + "</div>"));
          });
        }
        state = $("<div class='state'></div>");
        step.append(state);
        addStateContents(state, obj.reached_states[item[0]][1]);
      });

      obj.present_powers.forEach(function(item, index) {
        var step_div = $("div.step" + item[0]);
        item[1].forEach(function(tagged) {
          step_div.find('div.tagged-elem:contains(\'' + tagged.textual + "\')").addClass("power-active");
        });
      });

      obj.present_duties.forEach(function(item, index) {
        var step_div = $("div.step" + item[0]);
        item[1].forEach(function(duty_pair) {
          var te = step_div.find('div.tagged-elem:contains(\'' + duty_pair[0].textual + "\')");
          if (duty_pair[1])
            te.addClass("duty-violated");
          else
            te.addClass("duty-active");
        });
      });

      steps.find("div.action").click(function(item) {
        $(this).closest(".step").find(".state").toggle(100);
      });

      str = JSON.stringify(obj, undefined, 4);
      $("#command").html("<h2>command output (hidden)</h2><pre class='toggleitem'>"+str+"</pre>");
      $("#command").find("pre").hide();
      $("#command").find("h2").click(function() {
         $(this).parent().find("pre").toggle();
      });
	  }
	  
    }, "json").fail(function(xhr, status, error) {
 	    response_field.html("technical error");
      $("#command").html("" + error);
    });
  });
  
  $("#reset").click(function() {
    $("#code_model").val("");    
    $("#code_refinement").val("");
    $("#code_initial_state").val("");
    $("#code_scenario").val("");
    $("#console").val("");
    $("#result").html("");    
    $("div.step").remove();
    $("#response").find("pre").html("");
  });  
  
  $(".program").click(function() {
    var file = $(this).attr('rel');
    startFileLoad(file);
  });
   
  $("#fileToUpload").change(function() {
    $(this).closest("form").find("#load_submit").click();
  }); 

});
</script>

<STYLE type="text/css">
html, body, * { margin: 0px; padding: 0px; }

body { padding: 20px; }
h1 {margin: 0px 0px 10px; }
h2 {margin: 20px 0px 10px; }
.left { width: 49%; float: left;}
.right { width: 49%; float: right; background-color: white; }
.clear { clear: both; width: 1px; line-height: 1px; }
.sticky { 
	position: -webkit-sticky; 
	position: sticky; 
	top: 0; 
}
.transparent { 
	opacity: 0.2; 
	filter: alpha(opacity=20); 
}
textarea { padding: 5px; width: 100%; background-color:#eeeeee; border:solid 1px #ccc; }
input { height: 30px; margin: 0px 0px; padding: 2px; margin-right: 5px; }
#commands { height: 35px; margin-top: 10px; display: block; }
span#align { vertical-align: top; margin: 0px 5px; }
#example_box { margin-top: 10px; }

pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }
.string { color: green; }
.number { color: darkorange; }
.boolean { color: blue; }
.null { color: magenta; }
.key { color: red; }

body {
  font-size: 15px;
  line-height: 1.5; 
  font-family: 'Helvetica Neue', Helvetica, Arial, serif;
  font-weight: 400;
  color: #666;
}

a {
  color: #2879d0;
  text-decoration: none;
}
a:hover {
  color: #2268b2;
}

.green {
  color: #28D22B;
}

.red {
  color: #D22B28;
}

div.step {
  margin-bottom: 1em;
}

div.action > div.instance{
  background-color:lightblue;
  color:purple;
  text-decoration: underline;
}

div.action, #command > h2 {
  cursor: cell;
}

div.created, div.terminated {
  margin-left: 1em;
}

div.created {
  background-color: lightgreen;
}

div.terminated{
  background-color: orange;
}

div.state {
  margin-left: 2em;
  display:none; 
}

div.state div.tagged-elem {
  color: gray;
}

div.state div.tagged-elem.power-active, div.state div.tagged-elem.duty-active {
  color: black;
}

div.state div.tagged-elem.duty-active, div.state div.tagged-elem.duty-violated {
  font-style: italic;
}

div.state div.tagged-elem.duty-violated {
  background-color: red;
  color: yellow;
}

</STYLE>
</head> 
<body> 

<h1><a href='index.php'>eFLINT online!</a></h1>
<hr/>

<div class='right'>

<div id='examples_box'>
<h2 id='showexamples'>examples</h2>
<span id='examples'>
Scenarios: 
<a href="#" class='program' rel='examples/vehicles.eflint'>Vehicles</a> | 
<a href="#" class='program' rel='examples/departments3.eflint'>Departments</a> | 
<a href="#" class='program' rel='examples/count_votes.eflint'>Count Votes</a> | 
<a href="#" class='program' rel='examples/cast_votes.eflint'>Cast Votes</a> | 
<a href="#" class='program' rel='examples/buyer_seller_v1.eflint'>Buyer/Seller (v1)</a> | 
<a href="#" class='program' rel='examples/buyer_seller_v2.eflint'>Buyer/Seller (v2)</a> | 
<a href="#" class='program' rel='examples/buyer_seller_v3.eflint'>Buyer/Seller (v3)</a> | 
<a href="#" class='program' rel='examples/permits.eflint'>Permit Applications</a> | 
<a href="#" class='program' rel='examples/permits_with_duty.eflint'>Permit Applications (v2)</a> | 
<a href="#" class='program' rel='examples/multiple_taxpayers_1.eflint'>Multiple taxpayers</a> | 
<a href="#" class='program' rel='examples/voting_full.eflint'>Voting</a> | 
</span>
</div>
<div>
<form action="index.php" method="post" enctype="multipart/form-data">
  Load file: <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="submit" value="Load" name="submit" style='display:none' id='load_submit'>
</form>
</div>
<form action="save.php" method="post" >
<div id='scenario'>
<h2>scenario</h2>
<textarea id='code_scenario' name="code_scenario" rows="10" cols="70"><?php 
if(isset($code_scenario)) 
  echo $code_scenario;
else
  echo "//place your scenario definition here"; 
?></textarea>
</div>
<div id='commands'>
<input type="button" id='run' value="Run"> 
<input type="button" id='reset' value="Reset"> 
<input type="submit" type="button"  value="Save"> 
<span id='align'>model name</span> <input type="text" name="filename">   
<!-- <input type="button" value="Reset" onclick='location.href="";'> -->
</div>
<div id='response'>
  <h2>response</h2>
  <pre style='width:100%'/>
</div>
<div id='output'>
  <h2>output</h2>
</div>
<div id='command'>
</div>
</div>

<div class='left'>
<h2>frames</h2>
<textarea id='code_model' name="code_model" rows="30" cols="70"><?php 
if (isset($code_model))
   echo $code_model; 
else
  echo "//place your fact-type, act-type and duty-type declarations here"; 
?></textarea>

<h2>domains</h2>
<textarea id='code_refinement' name="code_refinement" rows="11" cols="70"><?php
if (isset($code_refinement))
  echo $code_refinement;
else
  echo "//you can redefine fact-types here to given them narrower scope (less instances)\n//this allows you to zoom in on the case analyzed by the scenario"; 
?></textarea>

<h2>initial state</h2>
<textarea id='code_initial_state' name="code_initial_state" rows="10" cols="70"><?php
if (isset($code_initial_state))
  echo $code_initial_state;
else
  echo "//define the initial state here"; 
?></textarea>

</div>

<div class='clear'>&nbsp;</div>

<div id='result'>
</div>

</form>
</body>

</html>

